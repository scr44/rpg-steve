﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Menus
{
    [TestClass]
    public class StartupMenus
    {
        [TestMethod]
        public void MainMenuSelection()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void NewGameSelection()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void ScenarioSelection()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void ProfessionSelection()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void CustomProfessionCreation()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void UserInputCharacterData()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void StartNewGame()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void LoadSavedGame()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void ProgramOptionsSelection()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void QuitProgramSelection()
        { Assert.IsTrue(false); }
    }
    [TestClass]
    public class PauseMenu
    {
        [TestMethod]
        public void SaveGame()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void LoadGame()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void QuitToMain()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void QuitGame()
        { Assert.IsTrue(false); }
    }
    [TestClass]
    public class CharacterMenu
    {
        [TestMethod]
        public void CharacterStatPage()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void StartLevelUpMenu()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void ChangeMenuPage()
        {
            Assert.IsTrue(false);
        }
    }
    [TestClass]
    public class EquipmentMenu
    {
        [TestMethod]
        public void DisplayEquipment()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void MainHand()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void OffHand()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void Body()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void Charm1()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void Charm2()
        { Assert.IsTrue(false); }
    }
    [TestClass]
    public class InventoryMenu
    {
        [TestMethod]
        public void ScrollInventory()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void UseItem()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void DropItem()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void SortByName()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void SortByValue()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void SortByNumber()
        { Assert.IsTrue(false); }
        [TestMethod]
        public void SortByType() // Consumables, Equipment, Vendor Trash
        { Assert.IsTrue(false); }
    }
}
